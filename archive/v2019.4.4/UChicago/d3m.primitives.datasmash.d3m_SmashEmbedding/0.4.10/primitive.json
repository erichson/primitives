{
    "algorithm_types": [
        "HIDDEN_MARKOV_MODEL",
        "RANDOM_WALK",
        "VARIABLE_ORDER_MARKOV_MODEL"
    ],
    "name": "datasmash.d3m_SmashEmbedding",
    "primitive_family": "EVALUATION",
    "python_path": "d3m.primitives.datasmash.d3m_SmashEmbedding",
    "source": {
        "name": "UChicago",
        "contact": "mailto:virotaru@uchicago.edu",
        "uris": [
            "https://gitlab.datadrivendiscovery.org/uchicago/datasmash/datasmash/d3m_embedding.py",
            "https://gitlab.datadrivendiscovery.org/uchicago/datasmash.git"
        ]
    },
    "version": "0.4.10",
    "id": "1df5f748-122f-45a7-aff8-46cf5332c097",
    "installation": [
        {
            "type": "PIP",
            "package": "datasmash",
            "version": "0.4.10"
        }
    ],
    "hyperparameters_to_tune": [
        "n_components"
    ],
    "keywords": [
        "time",
        "series",
        "data smashing",
        "data-smashing",
        "data_smashing",
        "datasmashing",
        "feature construction",
        "embedembedding",
        "featurization",
        "parameter-free",
        "hyperparameter-free"
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "datasmash.d3m_embedding.d3m_SmashEmbedding",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.dataset.Dataset",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "datasmash.d3m_embedding.Params",
            "Hyperparams": "datasmash.d3m_embedding.Hyperparams"
        },
        "interfaces_version": "2019.4.4",
        "interfaces": [
            "featurization.FeaturizationLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "n_components": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 2,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of features to create from each time series. "
            },
            "quantizer_core_hours": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 28,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter",
                    "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
                ],
                "description": "Number of core-hours one wishes to allot to the QUANTIZATION PORTION of the algorithm.",
                "lower": 1,
                "upper": null
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "datasmash.d3m_embedding.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.dataset.Dataset",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "datasmash.d3m_embedding.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "datasmash.d3m_embedding.Hyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "datasmash.d3m_embedding.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Parameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {}
    },
    "structural_type": "datasmash.d3m_embedding.d3m_SmashEmbedding",
    "description": "A base class for primitives.\n\nClass is parameterized using four type variables, ``Inputs``, ``Outputs``, ``Params``,\nand ``Hyperparams``.\n\n``Params`` has to be a subclass of `d3m.metadata.params.Params` and should define\nall fields and their types for parameters which the primitive is fitting.\n\n``Hyperparams`` has to be a subclass of a `d3m.metadata.hyperparams.Hyperparams`.\nHyper-parameters are those primitive's parameters which primitive is not fitting and\ngenerally do not change during a life-time of a primitive.\n\n``Params`` and ``Hyperparams`` have to be pickable and copyable. See `pickle`,\n`copy`, and `copyreg` Python modules for more information.\n\nIn this context we use term method arguments to mean both formal parameters and\nactual parameters of a method. We do this to not confuse method parameters with\nprimitive parameters (``Params``).\n\nAll arguments to all methods are keyword-only. No ``*args`` or ``**kwargs`` should\never be used in any method.\n\nStandardized interface use few public attributes and no other public attributes are\nallowed to assure future compatibility. For your attributes use the convention that\nprivate symbols should start with ``_``.\n\nPrimitives can have methods which are not part of standardized interface classes:\n\n* Additional \"produce\" methods which are prefixed with ``produce_`` and have\n  the same semantics as ``produce`` but potentially return different output\n  container types instead of ``Outputs`` (in such primitive ``Outputs`` is seen as\n  primary output type, but the primitive also has secondary output types).\n  They should return ``CallResult`` and have ``timeout`` and ``iterations`` arguments.\n* Private methods prefixed with ``_``.\n\nNo other public additional methods are allowed. If this represents a problem for you,\nopen an issue. (The rationale is that for other methods an automatic system will not\nunderstand the semantics of the method.)\n\nMethod arguments which start with ``_`` are seen as private and can be used for arguments\nuseful for debugging and testing, but they should not be used by (or even known to) a\ncaller during normal execution. Such arguments have to be optional (have a default value)\nso that the method can be called without the knowledge of the argument.\n\nAll arguments to all methods and all hyper-parameters together are seen as arguments to\nthe primitive as a whole. They are identified by their names. This means that any argument\nname must have the same type and semantics across all methods, effectively be the same argument.\nIf a method argument matches in name a hyper-parameter, it has to match it in type and semantics\nas well. Such method argument overrides a hyper-parameter for a method call. All this is necessary\nso that callers can have easier time determine what values to pass to arguments and that it is\neasier to describe what all values are inputs to a primitive as a whole (set of all\narguments).\n\nTo recap, subclasses can extend arguments of standard methods with explicit typed keyword\narguments used for the method call, or define new \"produce\" methods with arbitrary explicit\ntyped keyword arguments. There are multiple kinds of such arguments allowed:\n\n* An (additional) input argument of any container type and not necessary of ``Inputs``\n  (in such primitive ``Inputs`` is seen as primary input type, but the primitive also has\n  secondary input types).\n* An argument which is overriding a hyper-parameter for the duration of the call.\n  It should match a hyper-parameter in name and type. It should be a required argument\n  (no default value) which the caller has to supply (or with a default value of a\n  hyper-parameter, or with the same hyper-parameter as it was passed to the constructor,\n  or with some other value). This is meant just for fine-control by a caller during fitting\n  or producing, e.g., for a threshold or learning rate, and is not reasonable for most\n  hyper-parameters.\n* An (additional) value argument which is one of standard data types, but not a container type.\n  In this case a caller will try to satisfy the input by creating part of a pipeline which\n  ends with a primitive with singleton produce method and extract the singleton value and\n  pass it without a container. This kind of an argument is **discouraged** and should probably\n  be a hyper-parameter instead (because it is unclear how can a caller determine which value\n  is a reasonable value to pass in an automatic way), but it is defined for completeness and\n  so that existing pipelines can be easier described.\n* A private argument prefixed with ``_`` which is used for debugging and testing.\n  It should not be used by (or even known to) a caller during normal execution.\n  Such argument has to be optional (have a default value) so that the method can be called\n  without the knowledge of the argument.\n\nEach primitive's class automatically gets an instance of Python's logging logger stored\ninto its ``logger`` class attribute. The instance is made under the name of primitive's\n``python_path`` metadata value. Primitives can use this logger to log information at\nvarious levels (debug, warning, error) and even associate extra data with log record\nusing the ``extra`` argument to the logger calls.\n\nSubclasses of this class allow functional compositionality.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "460e851cedf9dc5ecbb1bb611506e4cdf864ba258adf2e3229bd1f315e1b47a2"
}
